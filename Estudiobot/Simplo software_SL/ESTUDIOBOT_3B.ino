//Estudiobot - Inicialmente apagar motores, esperar 30 segundos y repetir este ciclo:
//             activar motor izquierdo en sentido retroceso con PWM = 80 durante 1 
//             segundo, activarlo en sentido retroceso con PWM = 255 durante 1 segundo,
//             repetir el ciclo.

#define sal_RDB  7  //pin motor izquierdo B
#define sal_RIB  8  //pin motor derecho   B
#define sal_RDA  9  //pin motor izquierdo A (PWM)
#define sal_RIA 10  //pin motor derecho   A (PWM)

int vel_I;

void setup()
{
  pinMode(sal_RIB, OUTPUT);
  pinMode(sal_RDB, OUTPUT);
  pinMode(sal_RIA, OUTPUT);
  pinMode(sal_RDA, OUTPUT);
  digitalWrite(sal_RIB, LOW);
  digitalWrite(sal_RDB, LOW);
  digitalWrite(sal_RIA, LOW);
  digitalWrite(sal_RDA, LOW);
  delay(30000);
}
  

void loop()
{
  vel_I = 80;         //define PWM = 80 para motor izquierdo
  RETROCEDER_IZQ();   //mueve motor izquierdo en sentido retroceso
  ESPERAR(1000);      //espera 1 segundo          
  vel_I = 255;        //define PWM = 255 para motor izquierdo
  RETROCEDER_IZQ();   //mueve motor izquierdo en sentido retroceso
  ESPERAR(1000);      //espera 1 segundo           
}  


void RETROCEDER_IZQ()
{
  analogWrite (sal_RIA, vel_I);   
  digitalWrite(sal_RIB, LOW);       
}


void ESPERAR(int tiempo)
{
  delay(tiempo);         
}



