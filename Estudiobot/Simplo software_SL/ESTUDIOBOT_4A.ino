//Estudiobot - Inicialmente apagar motores, esperar 30 segundos y repetir este ciclo:
//             activar motor izquierdo en sentido avance con PWM = 80 durante 1 
//             segundo, activarlo en sentido avance con PWM = 255 durante 1 segundo,
//             repetir el ciclo.

#define sal_RDB  7  //pin motor izquierdo B
#define sal_RIB  8  //pin motor derecho   B
#define sal_RDA  9  //pin motor izquierdo A (PWM)
#define sal_RIA 10  //pin motor derecho   A (PWM)

void setup()
{
  pinMode(sal_RIB, OUTPUT);
  pinMode(sal_RDB, OUTPUT);
  pinMode(sal_RIA, OUTPUT);
  pinMode(sal_RDA, OUTPUT);
  digitalWrite(sal_RIB, LOW);
  digitalWrite(sal_RDB, LOW);
  digitalWrite(sal_RIA, LOW);
  digitalWrite(sal_RDA, LOW);
  delay(30000);
}
  

void loop()
{
  analogWrite (sal_RIA, 175);    //activa sal_RDA con PWM = 175 (255 - 80)
  digitalWrite(sal_RIB, HIGH);   //desactiva sal_RDA, 175 - HIGH avanza a veloc baja
  delay(1000);                   //espera 1000 mseg (retrocede a 80 durante 1000 mseg)
  analogWrite (sal_RIA, 0);      //activa sal_RDA con PWM = 0 (255 - 255)
  digitalWrite(sal_RIB, HIGH);   //desactiva sal_RDA, 0 - HIGH avanza a alta velocidad
  delay(1000);                   //espera 1000 mseg (retrocede a 255 durante 1000 mseg)
}                              //repite el ciclo

