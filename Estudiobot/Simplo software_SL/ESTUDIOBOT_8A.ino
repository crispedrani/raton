//Estudiobot - Lee el sensor y activa el led si algun objeto esta a menos de 20 cm
//             prender el led indicador, si no mantenerlo apagado.
//             uso del IF ELSE (si se cumple una condicion ejecutar A si no ejecutar B)

#define end_ECO 11  //entrada digital que lee el pulso devuelto por el sensor
#define sal_TRI 12  //salida que le indica al sensor que emita pulso de sonido ultrason
#define sal_LED 13  //salida del led indicador del Arduino

void setup()
{
  pinMode(end_ECO, INPUT);
  pinMode(sal_TRI, OUTPUT);
}

/////////////////////////////////////////////////////////////////////////////////////////

void loop()
{
  if (DIST_MEDIDA()<20)
    digitalWrite(sal_LED, HIGH);
  else
    digitalWrite(sal_LED, LOW);
} 
                                
/////////////////////////////////////////////////////////////////////////////////////////

int DIST_MEDIDA()
{
  digitalWrite(sal_TRI, LOW);                
  delayMicroseconds(5);                      
  digitalWrite(sal_TRI, HIGH);               
  delayMicroseconds(10);                     
  digitalWrite(sal_TRI, LOW);                
  int distancia = int (pulseIn (end_ECO, HIGH) / 58);    
  return distancia;
}

