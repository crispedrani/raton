//Estudiobot - Activar la salida 13 (led indicador Led1) por 500 mseg y desactivarla por
//             1000 mseg, repetir el ciclo.

#define sal_LED 13

void setup() 
{                                //aquí comienza el bloque del Setup
  pinMode(sal_LED, OUTPUT);      //pinMode configura pin D13 como salida.
}


void loop() 
{
  ACTIVAR_LED1_DURANTE(500);     //prende el Led1 durante 500 mseg y luego lo apaga
  ESPERAR(1000);                 //espera 1000 mseg
}                                //fin del bloque, ejecuta 1ra instrucción del Loop


void ACTIVAR_LED1_DURANTE(int tiempo)
{
  digitalWrite(sal_LED, HIGH);
  delay(tiempo);
  digitalWrite(sal_LED, LOW);
}


void ESPERAR(int tiempo)
{
  delay(tiempo);         
}

