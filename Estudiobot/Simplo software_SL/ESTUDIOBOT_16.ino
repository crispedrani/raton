//Estudiobot - 4 programas - selección con mano y sensor ultrasónico

#define sal_RDB  7  //rueda derecha   B
#define sal_RIB  8  //rueda izquierda B
#define sal_RDA  9  //rueda derecha   A  salida PWM
#define sal_RIA 10  //rueda izquierda A  salida PWM

#define end_ECO 11  //ECHO del ultrasonico
#define sal_TRI 12  //TRIGGER del ultrasonico
#define sal_LED 13  //pin 13 - led indicador

int vel_I, vel_D, numprog, tiempo;

void setup()
{
  pinMode(sal_RIA, OUTPUT);       //salida A motor izquierdo
  pinMode(sal_RIB, OUTPUT);       //salida B motor izquierdo
  pinMode(sal_RDA, OUTPUT);       //salida A motor derecho
  pinMode(sal_RDB, OUTPUT);       //salida B motor derecho 

  pinMode(end_ECO, INPUT);        //entrada pin ECHO del ultrasonico
  pinMode(sal_TRI, OUTPUT);       //salida  pin TRIGGER del ultrasonico

  pinMode(sal_LED, OUTPUT);       //salida pin 13 - led indicador

  digitalWrite(sal_RIA, LOW);     //inicialización de salidas de motor, apaga todas
  digitalWrite(sal_RIB, LOW);
  digitalWrite(sal_RDA, LOW);
  digitalWrite(sal_RDB, LOW);

  for(int cont=0; cont<3; cont++) //indicación de inicio/reset del Arduino
  {                               //3 parpadeos
    ACTIVAR_LED1_DURANTE(200);
    ESPERAR(200);
  }
  while(OBJ_A_MENOS_DE(20));    //el programa se detiene acá por si se enciende el robot
                                //con algo delante
  delay(500);                             
  while(!OBJ_A_MENOS_DE(20));   //espera a detectar
  while(OBJ_A_MENOS_DE(20))
    ACTIVAR_LED1();             //1ra detección (rutina 1)
  DESACTIVAR_LED1();
  numprog = 1;
  tiempo = 200;

  while(tiempo)
  {
    if (OBJ_A_MENOS_DE(20))
    { 
      while(OBJ_A_MENOS_DE(20))
        ACTIVAR_LED1();  
      DESACTIVAR_LED1();
      numprog++;
      tiempo = 200;
    }
    delay(5);
    tiempo--;
  }//del while
  if (numprog>4)
    numprog = 4;
    
  for (int cont=0; cont<numprog; cont++)
    {
    ACTIVAR_LED1_DURANTE(150);
    delay(400);
    }
  delay(500);
   
  //inicialización para cada rutina
  switch(numprog)
  {
    case 1:   vel_I = 100;
              vel_D = 100;
              break;
              
    case 2:   vel_I = 100;
              vel_D = 100;
              break;
    
    case 3:   vel_I = 100;
              vel_D = 100;
              break;
    
    default:  vel_I = 100;
              vel_D = 100; 
              break;
  } 
}
  
void loop()
{
  switch(numprog)
  {
    case 1:   rutina1();
              break;
              
    case 2:   rutina2();
              break;
    
    case 3:   rutina3();
              break;
    
    default:  rutina4();
              break;
  } 
}


int DIST_MEDIDA()  
{
  digitalWrite(sal_TRI, LOW);                
  delayMicroseconds(5);                      
  digitalWrite(sal_TRI, HIGH);               
  delayMicroseconds(10);                     
  digitalWrite(sal_TRI, LOW);                
  int distancia = int (pulseIn (end_ECO, HIGH) / 58);    
  return distancia;
}


bool OBJ_A_MENOS_DE(int distancia)  
{
  if (DIST_MEDIDA() < distancia)
  {
    delay(100);
    if (DIST_MEDIDA() < distancia)  //2da comprobación con nueva medición
      return 1;
    else
      return 0;
  }
  else
    return 0;
}


void AVANZAR_IZQ()
{
  analogWrite (sal_RIA, 255-vel_I);   
  digitalWrite(sal_RIB, HIGH);       
}


void RETROCEDER_IZQ()
{
  analogWrite (sal_RIA, vel_I);   
  digitalWrite(sal_RIB, LOW);       
}


void DETENER_IZQ()
{
  analogWrite (sal_RIA, 0);   
  digitalWrite(sal_RIB, LOW);    
}


void AVANZAR_DER()
{
  analogWrite (sal_RDA, 255-vel_D);   
  digitalWrite(sal_RDB, HIGH);        
}


void RETROCEDER_DER()
{
  analogWrite (sal_RDA, vel_D);   
  digitalWrite(sal_RDB, LOW);        
}


void DETENER_DER()
{
  analogWrite (sal_RDA, 0);   
  digitalWrite(sal_RDB, LOW);  
}


void ESPERAR(int tiempo)
{
  delay(tiempo);         
}


void AVANZAR()
{
  AVANZAR_IZQ();
  AVANZAR_DER();
}


void RETROCEDER()
{
  RETROCEDER_IZQ();
  RETROCEDER_DER();        
}      


void DETENER()
{
  DETENER_IZQ();
  DETENER_DER();
}


void GIRAR_H()
{
  AVANZAR_IZQ();
  RETROCEDER_DER();
}


void GIRAR_AH()
{
  RETROCEDER_IZQ();
  AVANZAR_DER();
}


void DOBLAR_DER()
{
  AVANZAR_IZQ();
  DETENER_DER();
}


void DOBLAR_IZQ()
{
  AVANZAR_DER();
  DETENER_IZQ();
}


void AVANZAR_DURANTE(int tiempo)
{
  AVANZAR();
  ESPERAR(tiempo);
  DETENER();
}


void RETROCEDER_DURANTE(int tiempo)
{
  RETROCEDER();
  ESPERAR(tiempo);
  DETENER();      
}      


void DETENER_DURANTE(int tiempo)
{
  DETENER();
  ESPERAR(tiempo);
}


void GIRAR_H_DURANTE(int tiempo)
{
  GIRAR_H();
  ESPERAR(tiempo);
  DETENER();
}


void GIRAR_AH_DURANTE(int tiempo)
{
  GIRAR_AH();
  ESPERAR(tiempo);
  DETENER();
}


void DOBLAR_DER_DURANTE(int tiempo)
{
  DOBLAR_DER();
  ESPERAR(tiempo);
  DETENER();
}


void DOBLAR_IZQ_DURANTE(int tiempo)
{
  DOBLAR_IZQ();
  ESPERAR(tiempo);
  DETENER();
}


void ACTIVAR_LED1()
{
  digitalWrite(sal_LED, HIGH);
}


void DESACTIVAR_LED1()
{
  digitalWrite(sal_LED, LOW);
}


void ACTIVAR_LED1_DURANTE(int tiempo)
{
  digitalWrite(sal_LED, HIGH);
  ESPERAR(tiempo);
  digitalWrite(sal_LED, LOW);
}


void DESACTIVAR_LED1_DURANTE(int tiempo)
{
  digitalWrite(sal_LED, LOW);
  ESPERAR(tiempo);
  digitalWrite(sal_LED, HIGH);
}


/////////////////////////////////////////////////////////////////////////////////////////

void rutina1()                 //avanza y esquiva
{
  if (OBJ_A_MENOS_DE(20))      //si detecta un objeto a menos de 20 cm
  {
    ACTIVAR_LED1();            //prende el led
    RETROCEDER_DURANTE(200);   //retrocede un instante
    GIRAR_AH_DURANTE(400);     //gira en el lugar en sentido antihorario
  }
  else                         //si no detecta nada
  {
    DESACTIVAR_LED1();         //apaga el led
    AVANZAR();                 //y avanza
  }  
}


void rutina2()                 //se esconde en rincones
{
  AVANZAR();                   //avanza
  if (OBJ_A_MENOS_DE(20))      //si detecta un objeto a menos de 20 cm
  {
    ACTIVAR_LED1;              //prende el led
    GIRAR_H_DURANTE(700);      //y hace una media vuelta en el lugar
    DETENER_DURANTE(500);      //se detiene y hace una pausa
  }
  DESACTIVAR_LED1();           //apaga el led
  while(!OBJ_A_MENOS_DE(100)); //y se queda así hasta que detecte un objeto a menos de 1m  
}                              //en ese caso repite el ciclo


void rutina3()                 //empuja cajas
{
  if(OBJ_A_MENOS_DE(35))       //si detecta un objeto a menos de 35 cm
  {
    ACTIVAR_LED1();            //prende el led 
    GIRAR_H_DURANTE(100);      //gira un poco más para quedar bien enfrentado
    AVANZAR_DURANTE(1000);     //avanza hacia el objeto para empujarlo
    RETROCEDER_DURANTE(1000);  //y retrocede
    GIRAR_H_DURANTE(200);      //hace una pausa
  }
  else                         //si no detecta nada
  {
    DESACTIVAR_LED1;           //apaga el led
    GIRAR_H_DURANTE(100);      //gira 
    DETENER_DURANTE(100);      //y para un instante, asi gira haciendo movimientos cortos
  } 
}


void rutina4()
{
  if (OBJ_A_MENOS_DE(30))      //si detecta un objeto a menos de 30
  {
    ACTIVAR_LED1();            //prende el led
    RETROCEDER();              //y retrocede
  }
  else                         //si no detecta nada a menos de 30
  {
    DESACTIVAR_LED1();         //apaga led
    if (OBJ_A_MENOS_DE(45))    //y ve si detecta algo a menos de 45 
      AVANZAR();               //si detecta avanza
    else                       //si no detecta nada
      DETENER();               //se detiene
  }   
}

/////////////////////////////////////////////////////////////////////////////////////////
