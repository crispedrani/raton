//Estudiobot - Inicialmente apagar motores, esperar 30 segundos y repetir este ciclo:
//             si detecta algo a menos de 30 cm prende led indicador y mueve el robot adelante
//             500 milisegundos y para 200 milisegundos, repite hasta que no detecta, en ese 
//             caso apaga led indicador y gira 1 segundo en sentido antihorario y 1 segundo en
//             sentido horario.
           
#define sal_RIB  7
#define sal_RDB  8
#define sal_RIA  9
#define sal_RDA 10
#define end_ECO 11  //entrada digital que lee el pulso devuelto por el sensor
#define sal_TRI 12  //salida que le indica al sensor que emita pulso de sonido ultrason
#define sal_LED 13  //salida del led indicador del Arduino

int vel_I, vel_D;

void setup()
{
  pinMode(end_ECO, INPUT);
  pinMode(sal_TRI, OUTPUT);
  pinMode(sal_RIB, OUTPUT);
  pinMode(sal_RDB, OUTPUT);
  pinMode(sal_RIA, OUTPUT);
  pinMode(sal_RDA, OUTPUT);
  digitalWrite(sal_RIB, LOW);
  digitalWrite(sal_RDB, LOW);
  digitalWrite(sal_RIA, LOW);
  digitalWrite(sal_RDA, LOW);
  delay(30000);
  vel_I = 100;
  vel_D = 100;
}

/////////////////////////////////////////////////////////////////////////////////////////

void loop()
{
  if (OBJ_A_MENOS_DE(30))         //si detecta un objeto a menos de 30 cm
    {
      while(OBJ_A_MENOS_DE(30))    //mientras siga habiendo un obj a menos de 30 cm    
        {
        ACTIVAR_LED1();
        AVANZAR_DURANTE(500);    //hace este ciclo avanza durante 1 segundo 
        ESPERAR(200);            //                para durante 1 segundo
        }                         //si ya no hay un obj a menos de 30 sale del while
      DESACTIVAR_LED1();
      GIRAR_AH_DURANTE(1000);     //y gira en sentido antihorario durante 1 segundo
      GIRAR_H_DURANTE(1000);      //y luego gira durante 1 segundo en sentido horario
    }                             //fin del loop, vuelve a verificar el if
}                              
                                
/////////////////////////////////////////////////////////////////////////////////////////


int DIST_MEDIDA()
{
  digitalWrite(sal_TRI, LOW);                
  delayMicroseconds(5);                      
  digitalWrite(sal_TRI, HIGH);               
  delayMicroseconds(10);                     
  digitalWrite(sal_TRI, LOW);                
  int distancia = int (pulseIn (end_ECO, HIGH) / 58);    
  return distancia;
}


bool OBJ_A_MENOS_DE(int distancia)  
{
  if (DIST_MEDIDA() < distancia)
  {
    delay(100);
    if (DIST_MEDIDA() < distancia)  //2da comprobación con nueva medición
      return 1;
    else
      return 0;
  }
  else
    return 0;
}


void AVANZAR_IZQ()
{
  analogWrite (sal_RIA, 255-vel_I);   
  digitalWrite(sal_RIB, HIGH);       
}

void RETROCEDER_IZQ()
{
  analogWrite (sal_RIA, vel_I);   
  digitalWrite(sal_RIB, LOW);       
}

void DETENER_IZQ()
{
  analogWrite (sal_RIA, 0);   
  digitalWrite(sal_RIB, LOW);    
}

void AVANZAR_DER()
{
  analogWrite (sal_RDA, 255-vel_D);   
  digitalWrite(sal_RDB, HIGH);        
}

void RETROCEDER_DER()
{
  analogWrite (sal_RDA, vel_D);   
  digitalWrite(sal_RDB, LOW);        
}

void DETENER_DER()
{
  analogWrite (sal_RDA, 0);   
  digitalWrite(sal_RDB, LOW);  
}

void ESPERAR(int tiempo)
{
  delay(tiempo);         
}

void AVANZAR()
{
  AVANZAR_IZQ();
  AVANZAR_DER();
}

void RETROCEDER()
{
  RETROCEDER_IZQ();
  RETROCEDER_DER();        
}      

void DETENER()
{
  DETENER_IZQ();
  DETENER_DER();
}

void GIRAR_H()
{
  AVANZAR_IZQ();
  RETROCEDER_DER();
}

void GIRAR_AH()
{
  RETROCEDER_IZQ();
  AVANZAR_DER();
}

void DOBLAR_DER()
{
  AVANZAR_IZQ();
  DETENER_DER();
}

void DOBLAR_IZQ()
{
  AVANZAR_DER();
  DETENER_IZQ();
}

void AVANZAR_DURANTE(int tiempo)
{
  AVANZAR();
  ESPERAR(tiempo);
  DETENER();
}

void RETROCEDER_DURANTE(int tiempo)
{
  RETROCEDER();
  ESPERAR(tiempo);
  DETENER();      
}      

void DETENER_DURANTE(int tiempo)
{
  DETENER();
  ESPERAR(tiempo);
}

void GIRAR_H_DURANTE(int tiempo)
{
  GIRAR_H();
  ESPERAR(tiempo);
  DETENER();
}

void GIRAR_AH_DURANTE(int tiempo)
{
  GIRAR_AH();
  ESPERAR(tiempo);
  DETENER();
}

void DOBLAR_DER_DURANTE(int tiempo)
{
  DOBLAR_DER();
  ESPERAR(tiempo);
  DETENER();
}

void DOBLAR_IZQ_DURANTE(int tiempo)
{
  DOBLAR_IZQ();
  ESPERAR(tiempo);
  DETENER();
}

void ACTIVAR_LED1()
{
  digitalWrite(sal_LED, HIGH);
}

void DESACTIVAR_LED1()
{
  digitalWrite(sal_LED, LOW);
}

void ACTIVAR_LED1_DURANTE(int tiempo)
{
  digitalWrite(sal_LED, HIGH);
  ESPERAR(tiempo);
  digitalWrite(sal_LED, LOW);
}

void DESACTIVAR_LED1_DURANTE(int tiempo)
{
  digitalWrite(sal_LED, LOW);
  ESPERAR(tiempo);
  digitalWrite(sal_LED, HIGH);
}

