//Estudiobot - Inicialmente apagar motores, esperar 30 segundos y repetir este ciclo:
//             activar motor izquierdo en un sentido durante 1 segundo, apagarlo por 1
//             segundo, activarlo en el otro sentido por 1 segundo, apagarlo por 1 
//             segundo y repetir el ciclo.

#define sal_RDB  7  //pin motor izquierdo B
#define sal_RIB  8  //pin motor derecho   B
#define sal_RDA  9  //pin motor izquierdo A
#define sal_RIA 10  //pin motor derecho   A

void setup()
  {
  pinMode(sal_RIB, OUTPUT); //configura pin sal_RIB como salida
  pinMode(sal_RDB, OUTPUT); //configura pin sal_RDB como salida
  pinMode(sal_RIA, OUTPUT); //configura pin sal_RIA como salida
  pinMode(sal_RDA, OUTPUT); //configura pin sal_RDA como salida
  digitalWrite(sal_RIB, LOW); 
  digitalWrite(sal_RDB, LOW); //desactiva salidas de motores
  digitalWrite(sal_RIA, LOW); 
  digitalWrite(sal_RDA, LOW);
  delay(30000); 
  }


void loop()
  {
  digitalWrite(sal_RIA, HIGH); //activa    sal_RIA
  digitalWrite(sal_RIB, LOW);  //desactiva sal_RIB, HIGH-LOW: motor gira en sentido 1
  delay(1000);                 //espera 1000 mseg (el motor gira por 1000 mseg)
  digitalWrite(sal_RIA, LOW);  //desactiva sal_RIA
  digitalWrite(sal_RIB, LOW);  //desactiva sal_RIB, LOW-LOW: apaga el motor
  delay(1000);                 //espera 1000 mseg (el motor permanece apagado 1000 mseg)
  digitalWrite(sal_RIA, LOW);  //desactiva sal_RIA
  digitalWrite(sal_RIB, HIGH); //activa    sal_RIB, HIGH-LOW: motor gira en sent 2
  delay(1000);                 //espera 1000 mseg (el motor gira por 1000 mseg)
  digitalWrite(sal_RIA, LOW);  //desactiva sal_RIA
  digitalWrite(sal_RIB, LOW);  //desactiva sal_RIB, LOW-LOW: apaga el motor
  delay(1000);                 //espera 1000 mseg (el motor permanece apagado 1000 mseg)
  }                            //fin del Loop, repite el ciclo

