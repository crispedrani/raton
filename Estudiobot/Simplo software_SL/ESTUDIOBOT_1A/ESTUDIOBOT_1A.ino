//Estudiobot - Activar la salida 13 (led indicador Led1) por 500 mseg y desactivarla por
//             1000 mseg, repetir el ciclo.

#define sal_LED 13

void setup() 
{                                //aquí comienza el bloque del Setup
  pinMode(sal_LED, OUTPUT);      //pinMode configura pin D13 como salida.
}


void loop() 
{
  digitalWrite(sal_LED, HIGH);   //activa la salida D13, prende el Led1 
  delay(500);                    //espera 500 mseg.
  digitalWrite(sal_LED, LOW);    //desactiva la salida D13, apaga el Led1
  delay(1000);                   //espera 1000 mseg.
}                                //fin del bloque, ejecuta 1ra instrucción del Loop


