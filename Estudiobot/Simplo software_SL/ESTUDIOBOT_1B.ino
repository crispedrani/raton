//Estudiobot - Activar la salida 13 (led indicador Led1) por 500 mseg y desactivarla por
//             1000 mseg, repetir el ciclo.

#define sal_LED 13

void setup() 
{                                //aquí comienza el bloque del Setup
  pinMode(sal_LED, OUTPUT);      //pinMode configura pin D13 como salida.
}


void loop() 
{
  ACTIVAR_LED1();        //activa la salida D13, prende el Led1 
  ESPERAR(500);          //espera 500 mseg.
  DESACTIVAR_LED1();     //desactiva la salida D13, apaga el Led1
  ESPERAR(1000);         //espera 1000 mseg.
}                        //fin del bloque, ejecuta 1ra instrucción del Loop


void ACTIVAR_LED1()
{
  digitalWrite(sal_LED, HIGH);
}


void DESACTIVAR_LED1()
{
  digitalWrite(sal_LED, LOW);
}


void ESPERAR(int tiempo)
{
  delay(tiempo);         
}

