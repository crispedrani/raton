//Estudiobot - activar el sensor ultrasónico cada 2 segundos y mostrar con el led 
//             indicador el estado de la entrada end_ECO conectada al sensor.

#define end_ECO 11  //entrada digital que lee el pulso devuelto por el sensor
#define sal_TRI 12  //salida que le indica al sensor que emita pulso de sonido ultrason
#define sal_LED 13  //salida del led indicador del Arduino

void setup()
  {
  pinMode(end_ECO, INPUT);
  pinMode(sal_TRI, OUTPUT);
  }

void loop()
  {
  digitalWrite(sal_TRI, LOW);                //pone en bajo salida sal_TRI
  delayMicroseconds(5);                      //espera 5 microsegundos
  digitalWrite(sal_TRI, HIGH);               
  delayMicroseconds(10);                     //genera un pulso de 10 microsegundos
  digitalWrite(sal_TRI, LOW);                //en salida sal_TRI
  while(!digitalRead(end_ECO));              //mientras pin este en LOW repite el while
  while(digitalRead(end_ECO))                //mientras el pin este en HIGH
    digitalWrite(sal_LED, HIGH);             //prende el Led1, cuando deja de ser HIGH
  digitalWrite(sal_LED, LOW);                //apaga el Led1
  delay(2000);                               //hace una demora de 200
  }                                          //multiplicado por 4 para que se note
  



