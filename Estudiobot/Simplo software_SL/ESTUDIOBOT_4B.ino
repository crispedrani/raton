//Estudiobot - Inicialmente apagar motores, esperar 30 segundos y repetir este ciclo:
//             activar motor izquierdo en sentido avance con PWM = 80 durante 1 
//             segundo, activarlo en sentido avance con PWM = 255 durante 1 segundo,
//             repetir el ciclo.

#define sal_RDB  7  //pin motor izquierdo B
#define sal_RIB  8  //pin motor derecho   B
#define sal_RDA  9  //pin motor izquierdo A (PWM)
#define sal_RIA 10  //pin motor derecho   A (PWM)

int vel_I;

void setup()
  {
  pinMode(sal_RIB, OUTPUT);
  pinMode(sal_RDB, OUTPUT);
  pinMode(sal_RIA, OUTPUT);
  pinMode(sal_RDA, OUTPUT);
  digitalWrite(sal_RIB, LOW);
  digitalWrite(sal_RDB, LOW);
  digitalWrite(sal_RIA, LOW);
  digitalWrite(sal_RDA, LOW);
  delay(30000);
  }
  

void loop()
{
  vel_I = 80;
  AVANZAR_IZQ();   
  delay(1000);                  
  vel_I = 255;   
  AVANZAR_IZQ();   
  delay(1000);                  
}    


void AVANZAR_IZQ()
{
  analogWrite (sal_RIA, 255-vel_I);   
  digitalWrite(sal_RIB, HIGH);       
}


void ESPERAR(int tiempo)
{
  delay(tiempo);         
}

