//Estudiobot - Inicialmente apagar motores, esperar 30 segundos y repetir este ciclo:
//             doblar  a izquierda durante 1 segundo, doblar a derecha durante 1 segundo, 
//             avanzar durante 1 segundo, retroceder durante 1 segundo, girar sentido horario
//             durante 1 segundo, girar sentido antihorario durante 1 segundo, detener durante
//             1 segundo.

#define sal_RDB  7  //pin motor izquierdo B
#define sal_RIB  8  //pin motor derecho   B
#define sal_RDA  9  //pin motor izquierdo A (PWM)
#define sal_RIA 10  //pin motor derecho   A (PWM)

int vel_I,vel_D;

void setup()
{
  pinMode(sal_RIB, OUTPUT);
  pinMode(sal_RDB, OUTPUT);
  pinMode(sal_RIA, OUTPUT);
  pinMode(sal_RDA, OUTPUT);
  digitalWrite(sal_RIB, LOW);
  digitalWrite(sal_RDB, LOW);
  digitalWrite(sal_RIA, LOW);
  digitalWrite(sal_RDA, LOW);
  delay(30000);
  vel_I = 100;
  vel_D = 100;
}
  

void loop()
{
  DOBLAR_IZQ();
  ESPERAR(1000);
  DOBLAR_DER();
  ESPERAR(1000);
  AVANZAR();
  ESPERAR(1000);
  RETROCEDER();
  ESPERAR(1000);
  GIRAR_H();
  ESPERAR(1000);
  GIRAR_AH();
  ESPERAR(1000);
  DETENER();
  ESPERAR(1000);
}    


void AVANZAR_IZQ()
{
  analogWrite (sal_RIA, 255-vel_I);   
  digitalWrite(sal_RIB, HIGH);       
}


void RETROCEDER_IZQ()
{
  analogWrite (sal_RIA, vel_I);   
  digitalWrite(sal_RIB, LOW);       
}


void DETENER_IZQ()
{
  analogWrite (sal_RIA, 0);   
  digitalWrite(sal_RIB, LOW);    
}


void AVANZAR_DER()
{
  analogWrite (sal_RDA, 255-vel_D);   
  digitalWrite(sal_RDB, HIGH);        
}


void RETROCEDER_DER()
{
  analogWrite (sal_RDA, vel_D);   
  digitalWrite(sal_RDB, LOW);        
}


void DETENER_DER()
{
  analogWrite (sal_RDA, 0);   
  digitalWrite(sal_RDB, LOW);  
}


void ESPERAR(int tiempo)
{
  delay(tiempo);         
}


void AVANZAR()
{
  AVANZAR_IZQ();
  AVANZAR_DER();
}


void RETROCEDER()
{
  RETROCEDER_IZQ();
  RETROCEDER_DER();        
}      


void DETENER()
{
  DETENER_IZQ();
  DETENER_DER();
}


void GIRAR_H()
{
  AVANZAR_IZQ();
  RETROCEDER_DER();
}


void GIRAR_AH()
{
  RETROCEDER_IZQ();
  AVANZAR_DER();
}


void DOBLAR_DER()
{
  AVANZAR_IZQ();
  DETENER_DER();
}

void DOBLAR_IZQ()
{
  AVANZAR_DER();
  DETENER_IZQ();
}


