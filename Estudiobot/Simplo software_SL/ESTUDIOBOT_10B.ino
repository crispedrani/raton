//Estudiobot - Lee el sensor y cada vez que detecta un objeto prende el led, al dejar de 
//             detectarlo, lo parpadea 5 veces.
//             uso del WHILE (mientras se cumple la condición ejecuta lo q esta dentro 
//             del bloque siguiente, cada vez que termina revisa si se cumple y si es asi
//             vuelve a ejecutarlo si no sale y sigue)
//             uso del FOR para repetir una secuencia 5 veces.
           

#define end_ECO 11  //entrada digital que lee el pulso devuelto por el sensor
#define sal_TRI 12  //salida que le indica al sensor que emita pulso de sonido ultrason
#define sal_LED 13  //salida del led indicador del Arduino

void setup()
{
  pinMode(end_ECO, INPUT);
  pinMode(sal_TRI, OUTPUT);
}

/////////////////////////////////////////////////////////////////////////////////////////

void loop()
{
  if(OBJ_A_MENOS_DE(20))             //si detecta un objeto ejecuta el bloque del if
  {
    while(OBJ_A_MENOS_DE(20))        //mientras el objeto siga delante prende el led
      ACTIVAR_LED1();
    for(int cont=0 ; cont<5; cont++) //ejecuta al salir del while
    { 
      ACTIVAR_LED1_DURANTE(100);     //si ya no detecta sale del while y parpadea 5 veces
      ESPERAR(250);
    }
  }                                 
}                                   //termina el bloque del loop y vuelve al if.
                                
/////////////////////////////////////////////////////////////////////////////////////////


int DIST_MEDIDA()
{
  digitalWrite(sal_TRI, LOW);                
  delayMicroseconds(5);                      
  digitalWrite(sal_TRI, HIGH);               
  delayMicroseconds(10);                     
  digitalWrite(sal_TRI, LOW);                
  int distancia = int (pulseIn (end_ECO, HIGH) / 58);    
  return distancia;
}


bool OBJ_A_MENOS_DE(int distancia)  
{
  if (DIST_MEDIDA() < distancia)
  {
    delay(100);
    if (DIST_MEDIDA() < distancia)  //2da comprobación con nueva medición
      return 1;
    else
      return 0;
  }
  else
    return 0;
}

void ACTIVAR_LED1()
{
  digitalWrite(sal_LED, HIGH);
}

void DESACTIVAR_LED1()
{
  digitalWrite(sal_LED, LOW);
}

void ACTIVAR_LED1_DURANTE(int tiempo)
{
  digitalWrite(sal_LED, HIGH);
  delay(tiempo);
  digitalWrite(sal_LED, LOW);
}

void ESPERAR(int tiempo)
{
  delay(tiempo);
}

