//Estudiobot - Lee el sensor y prende el led indicador mas pausado o mas seguido según
//             la distancia a la que se encuentre un objeto delante.
//             uso del IF ELSE (si se cumple una condicion ejecutar A si no ejecutar B)

#define end_ECO 11  //entrada digital que lee el pulso devuelto por el sensor
#define sal_TRI 12  //salida que le indica al sensor que emita pulso de sonido ultrason
#define sal_LED 13  //salida del led indicador del Arduino

void setup()
{
  pinMode(end_ECO, INPUT);
  pinMode(sal_TRI, OUTPUT);
}

/////////////////////////////////////////////////////////////////////////////////////////

void loop()
{
  ACTIVAR_LED1_DURANTE(500);
  DESACTIVAR_LED1_DURANTE(DIST_MEDIDA()*3);  //multiplica por 3 para hacer mas notable
}                                            //la pausa según la distancia
                               
/////////////////////////////////////////////////////////////////////////////////////////

int DIST_MEDIDA()
{
  digitalWrite(sal_TRI, LOW);                
  delayMicroseconds(5);                      
  digitalWrite(sal_TRI, HIGH);               
  delayMicroseconds(10);                     
  digitalWrite(sal_TRI, LOW);                
  int distancia = int (pulseIn (end_ECO, HIGH) / 58);    
  return distancia;
}


bool OBJ_A_MENOS_DE(int distancia)  
{
  if (DIST_MEDIDA() < distancia)
  {
    delay(100);
    if (DIST_MEDIDA() < distancia)  //2da comprobación con nueva medición
      return 1;
    else
      return 0;
  }
  else
    return 0;
}


void ACTIVAR_LED1_DURANTE(int tiempo)
{
  digitalWrite(sal_LED, HIGH);
  delay(tiempo);
  digitalWrite(sal_LED, LOW);
}


void DESACTIVAR_LED1_DURANTE(int tiempo)
{
  digitalWrite(sal_LED, LOW);
  delay(tiempo);
  digitalWrite(sal_LED, HIGH);
}

