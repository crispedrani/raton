//Estudiobot - Inicialmente apagar motores, esperar 30 segundos y repetir este ciclo:
//             activar motor izquierdo en sentido retroceso con PWM = 80 durante 1 
//             segundo, activarlo en sentido retroceso con PWM = 255 durante 1 segundo,
//             repetir el ciclo.

#define sal_RDB  7  //pin motor izquierdo B
#define sal_RIB  8  //pin motor derecho   B
#define sal_RDA  9  //pin motor izquierdo A
#define sal_RIA 10  //pin motor derecho   A

void setup()
  {
  pinMode(sal_RIB, OUTPUT);
  pinMode(sal_RDB, OUTPUT);
  pinMode(sal_RIA, OUTPUT);
  pinMode(sal_RDA, OUTPUT);
  digitalWrite(sal_RIB, LOW);
  digitalWrite(sal_RDB, LOW);
  digitalWrite(sal_RIA, LOW);
  digitalWrite(sal_RDA, LOW);
  delay(30000);
  }
  

void loop()
  {
  analogWrite (sal_RIA, 80);    //activa sal_RIA con PWM = 80
  digitalWrite(sal_RIB, LOW);   //desactiva sal_RIA, 80 - LOW retrocede a veloc baja
  delay(1000);                  //espera 1000 mseg (retrocede a 80 durante 1000 mseg)
  analogWrite (sal_RIA, 255);   //activa sal_RIA con PWM = 255
  digitalWrite(sal_RIB, LOW);   //desactiva sal_RIA, 255 - LOW retrocede a alta velocidad
  delay(1000);                  //espera 1000 mseg (retrocede a 255 durante 1000 mseg)
  }                             //repite el ciclo

