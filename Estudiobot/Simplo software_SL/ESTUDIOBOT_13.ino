//Estudiobot - se esconde en rincón

#define sal_RDB  7  //rueda derecha   B
#define sal_RIB  8  //rueda izquierda B
#define sal_RDA  9  //rueda derecha   A  salida PWM
#define sal_RIA 10  //rueda izquierda A  salida PWM

#define end_ECO 11  //ECHO del ultrasonico
#define sal_TRI 12  //TRIGGER del ultrasonico
#define sal_LED 13  //pin 13 - led indicador

int vel_I, vel_D;

void setup()
{
  pinMode(sal_RIA, OUTPUT);       //salida A motor izquierdo
  pinMode(sal_RIB, OUTPUT);       //salida B motor izquierdo
  pinMode(sal_RDA, OUTPUT);       //salida A motor derecho
  pinMode(sal_RDB, OUTPUT);       //salida B motor derecho 

  pinMode(end_ECO, INPUT);        //entrada pin ECHO del ultrasonico
  pinMode(sal_TRI, OUTPUT);       //salida  pin TRIGGER del ultrasonico

  pinMode(sal_LED, OUTPUT);       //salida pin 13 - led indicador

  digitalWrite(sal_RIA, LOW);     //inicialización de salidas de motor, apaga todas
  digitalWrite(sal_RIB, LOW);
  digitalWrite(sal_RDA, LOW);
  digitalWrite(sal_RDB, LOW);

  for(int cont=0; cont<4; cont++) //indicación de inicio/reset del Arduino
  {
    ACTIVAR_LED1_DURANTE(200);
    ESPERAR(200);
  }
  while (!OBJ_A_MENOS_DE(20));    //espera a detectar para comenzar el programa
  ACTIVAR_LED1_DURANTE(1000);
  
  vel_I = 100;
  vel_D = 100;
}

/////////////////////////////////////////////////////////////////////////////////////////

void loop()
{
  AVANZAR();
  if (OBJ_A_MENOS_DE(20))
  {
    ACTIVAR_LED1;
    GIRAR_H_DURANTE(700);
    DETENER_DURANTE(500);
  }
  DESACTIVAR_LED1();
  while(!OBJ_A_MENOS_DE(100)); 
}
                                
/////////////////////////////////////////////////////////////////////////////////////////


int DIST_MEDIDA()  
{
  digitalWrite(sal_TRI, LOW);                
  delayMicroseconds(5);                      
  digitalWrite(sal_TRI, HIGH);               
  delayMicroseconds(10);                     
  digitalWrite(sal_TRI, LOW);                
  int distancia = int (pulseIn (end_ECO, HIGH) / 58);    
  return distancia;
}


bool OBJ_A_MENOS_DE(int distancia)  
{
  if (DIST_MEDIDA() < distancia)
  {
    delay(100);
    if (DIST_MEDIDA() < distancia)  //2da comprobación con nueva medición
      return 1;
    else
      return 0;
  }
  else
    return 0;
}


void AVANZAR_IZQ()
{
  analogWrite (sal_RIA, 255-vel_I);   
  digitalWrite(sal_RIB, HIGH);       
}

void RETROCEDER_IZQ()
{
  analogWrite (sal_RIA, vel_I);   
  digitalWrite(sal_RIB, LOW);       
}

void DETENER_IZQ()
{
  analogWrite (sal_RIA, 0);   
  digitalWrite(sal_RIB, LOW);    
}

void AVANZAR_DER()
{
  analogWrite (sal_RDA, 255-vel_D);   
  digitalWrite(sal_RDB, HIGH);        
}

void RETROCEDER_DER()
{
  analogWrite (sal_RDA, vel_D);   
  digitalWrite(sal_RDB, LOW);        
}

void DETENER_DER()
{
  analogWrite (sal_RDA, 0);   
  digitalWrite(sal_RDB, LOW);  
}

void ESPERAR(int tiempo)
{
  delay(tiempo);         
}

void AVANZAR()
{
  AVANZAR_IZQ();
  AVANZAR_DER();
}

void RETROCEDER()
{
  RETROCEDER_IZQ();
  RETROCEDER_DER();        
}      

void DETENER()
{
  DETENER_IZQ();
  DETENER_DER();
}

void GIRAR_H()
{
  AVANZAR_IZQ();
  RETROCEDER_DER();
}

void GIRAR_AH()
{
  RETROCEDER_IZQ();
  AVANZAR_DER();
}

void DOBLAR_DER()
{
  AVANZAR_IZQ();
  DETENER_DER();
}

void DOBLAR_IZQ()
{
  AVANZAR_DER();
  DETENER_IZQ();
}

void AVANZAR_DURANTE(int tiempo)
{
  AVANZAR();
  ESPERAR(tiempo);
  DETENER();
}

void RETROCEDER_DURANTE(int tiempo)
{
  RETROCEDER();
  ESPERAR(tiempo);
  DETENER();      
}      

void DETENER_DURANTE(int tiempo)
{
  DETENER();
  ESPERAR(tiempo);
}

void GIRAR_H_DURANTE(int tiempo)
{
  GIRAR_H();
  ESPERAR(tiempo);
  DETENER();
}

void GIRAR_AH_DURANTE(int tiempo)
{
  GIRAR_AH();
  ESPERAR(tiempo);
  DETENER();
}

void DOBLAR_DER_DURANTE(int tiempo)
{
  DOBLAR_DER();
  ESPERAR(tiempo);
  DETENER();
}

void DOBLAR_IZQ_DURANTE(int tiempo)
{
  DOBLAR_IZQ();
  ESPERAR(tiempo);
  DETENER();
}

void ACTIVAR_LED1()
{
  digitalWrite(sal_LED, HIGH);
}

void DESACTIVAR_LED1()
{
  digitalWrite(sal_LED, LOW);
}

void ACTIVAR_LED1_DURANTE(int tiempo)
{
  digitalWrite(sal_LED, HIGH);
  ESPERAR(tiempo);
  digitalWrite(sal_LED, LOW);
}

void DESACTIVAR_LED1_DURANTE(int tiempo)
{
  digitalWrite(sal_LED, LOW);
  ESPERAR(tiempo);
  digitalWrite(sal_LED, HIGH);
}

