//Estudiobot - Inicialmente apagar motores, esperar 30 segundos y repetir este ciclo:
//             avanzar robot durante 1 segundo, detenerse durante 1 segundo, retroceder durante
//             1 segundo, detenerse 1 segundo.

#define sal_RDB  7  //pin motor izquierdo B
#define sal_RIB  8  //pin motor derecho   B
#define sal_RDA  9  //pin motor izquierdo A (PWM)
#define sal_RIA 10  //pin motor derecho   A (PWM)

int vel_I,vel_D;

void setup()
{
  pinMode(sal_RIB, OUTPUT);
  pinMode(sal_RDB, OUTPUT);
  pinMode(sal_RIA, OUTPUT);
  pinMode(sal_RDA, OUTPUT);
  digitalWrite(sal_RIB, LOW);
  digitalWrite(sal_RDB, LOW);
  digitalWrite(sal_RIA, LOW);
  digitalWrite(sal_RDA, LOW);
  delay(30000);
}
  

void loop()
{
  vel_I = 80;
  vel_D = 80;
  AVANZAR_DURANTE(1000);     //robot avanza durante 1 segundo
  DETENER_DURANTE(1000);     //robot se detiene durante 1 segundo
  vel_I = 150;
  vel_D = 150;
  RETROCEDER_DURANTE(1000);  //robot retrocede durante 1 segundo
  DETENER_DURANTE(1000);     //robot se detiene durante 1 segundo
}                            //repite el ciclo


void AVANZAR_IZQ()
{
  analogWrite (sal_RIA, 255-vel_I);   
  digitalWrite(sal_RIB, HIGH);       
}


void RETROCEDER_IZQ()
{
  analogWrite (sal_RIA, vel_I);   
  digitalWrite(sal_RIB, LOW);       
}


void DETENER_IZQ()
{
  analogWrite (sal_RIA, 0);   
  digitalWrite(sal_RIB, LOW);    
}


void AVANZAR_DER()
{
  analogWrite (sal_RDA, 255-vel_D);   
  digitalWrite(sal_RDB, HIGH);        
}


void RETROCEDER_DER()
{
  analogWrite (sal_RDA, vel_D);   
  digitalWrite(sal_RDB, LOW);        
}


void DETENER_DER()
{
  analogWrite (sal_RDA, 0);   
  digitalWrite(sal_RDB, LOW);  
}


void AVANZAR()
{
  AVANZAR_IZQ();
  AVANZAR_DER();
}

void RETROCEDER()
{
  RETROCEDER_IZQ();
  RETROCEDER_DER();        
}      

void DETENER()
{
  DETENER_IZQ();
  DETENER_DER();
}


void AVANZAR_DURANTE(int tiempo)
{
  AVANZAR();
  ESPERAR(tiempo);
  DETENER();
}

void RETROCEDER_DURANTE(int tiempo)
{
  RETROCEDER();
  ESPERAR(tiempo);
  DETENER();        
}      

void DETENER_DURANTE(int tiempo)
{
  DETENER();
  ESPERAR(tiempo);
}


void ESPERAR(int tiempo)
{
  delay(tiempo);         
}                      

