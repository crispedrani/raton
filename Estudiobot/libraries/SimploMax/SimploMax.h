#ifndef SimploMax_h
#define SimploMax_h

#define REMOTEXY_MODE__ESP8266_HARDSERIAL_POINT
#include <RemoteXY.h>

//constante
#define cm_paso 20

//control remoto IR (modelo flechas abajo)
#define cod_0   0xFF9867
#define cod_1   0xFFA25D
#define cod_2   0xFF629D
#define cod_3   0xFFE21D
#define cod_4   0xFF22DD
#define cod_5   0xFF02FD
#define cod_6   0xFFC23D
#define cod_7   0xFFE01F
#define cod_8   0xFFA857
#define cod_9   0xFF906F
#define cod_AST 0xFF6897  //10
#define cod_NUM 0xFFB04F  //11
#define cod_ADE 0xFF18E7  //12
#define cod_ATR 0xFF4AB5  //13
#define cod_DER 0xFF5AA5  //14
#define cod_IZQ 0xFF10EF  //15
#define cod_OK  0xFF38C7  //16
#define     AST 10
#define     NUM 11
#define      OK 12
#define     ADE 13
#define     ATR 14
#define     IZQ 15
#define     DER 16

#include <IRremote.h>
#include <ir_Lego_PF_BitStreamEncoder.h>

//WIFI
#define REMOTEXY_SERIAL Serial
#define REMOTEXY_SERIAL_SPEED 115200
#define REMOTEXY_WIFI_SSID "Simplo_bot"
#define REMOTEXY_WIFI_PASSWORD "12345678"
#define REMOTEXY_SERVER_PORT 6377

#pragma pack(push, 1)
uint8_t RemoteXY_CONF[] =   // 58 bytes
  { 255,4,0,0,0,51,0,16,31,1,1,0,9,33,12,12,2,31,97,100,
  101,0,1,0,9,53,12,12,2,31,97,116,114,0,1,0,43,33,12,12,
  2,31,97,100,101,0,1,0,43,53,12,12,2,31,97,116,114,0 };
  
struct {
  uint8_t bot_izq_ade; // =1 con boton apretado
  uint8_t bot_izq_atr; 
  uint8_t bot_der_ade; 
  uint8_t bot_der_atr; 

  uint8_t connect_flag;

} RemoteXY;
#pragma pack(pop)


//control remoto IR
IRrecv infrarrojo(end_CIR);           
decode_results results; 


//variables 
int vel_I = 100, vel_D = 100, numprog=0, numrutina;
int ciclos, cont, pulsos_paso, pulsos_giro;
int operacion[30], valorop[30];
uint32_t comando;

//reset por control remoto
#define LARGO_TREN 32
// En microsegundos
#define LIMITE_BAJO 600         
#define LIMITE_ALTO 1800         
#define LIMITE_MAX 4000

long inicio, delta = 0, inicio2, delta2 = 0;
uint32_t valor_devuelto, valor = 0, valor2 = 0;
int pos = 0, pos2 = 0;
boolean ant, lectura;


//tonos
int c[5]={131,262,523,1046,2093};   // Do
int cs[5]={139,277,554,1108,2217};  // Do#
int d[5]={147,294,587,1175,2349};   // Re
int ds[5]={156,311,622,1244,2489};  // Re#
int e[5]={165,330,659,1319,2637};   // Mi
int f[5]={175,349,698,1397,2794};   // Fa
int fs[5]={185,370,740,1480,2960};  // Fa#
int g[5]={196,392,784,1568,3136};   // Sol
int gs[5]={208,415,831,1661,3322};  // Sol#
int a[5]={220,440,880,1760,3520};   // La
int as[5]={233,466,932,1866,3729};  // La#
int b[5]={247,494,988,1976,3951};   // Si

