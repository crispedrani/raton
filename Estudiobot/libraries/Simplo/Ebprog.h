#ifndef Simplo_h
#define Simplo_h

#define cod_AST 10
#define cod_NUM 11
#define cod_ARR 12
#define cod_IZQ 13
#define cod_OK  14
#define cod_DER 15
#define cod_ABA 16

//interrupcion para control remoto
 
#include "Arduino.h"
 
class Simplo
{
  public:
    int operacion[10], valor[10], cont, vel_D=100, vel_I=100;
    //uint32_t leido;
    //long inicio, delta;
    //int pos=0;
    //boolean has_value;
    Simplo(int pin_IR)
    void iniciar();
    void avanzar();
    void retroceder();
    void parar();
    void girar_h();
    void girar_ah();
    void doblar_der();
    void doblar_izq();
    void ajustar_velocidad (int vel);
    void avanzar_durante(int tiempo);
    void retroceder_durante(int tiempo);
    void parar_durante(int tiempo);
    void girar_h_durante(int tiempo);
    void girar_ah_durante(int tiempo);
    int  lectura_ultrasonico();
    bool objeto_a_menos_de(int distancia);
    void avanzar_hasta_obj_a(int distancia);
    void parar_hasta_obj_a(int distancia);
    void retroceder_hasta_obj_a_mas_de(int distancia);
    void girar_h_hasta_obj_a(int distancia);
    void girar_ah_hasta_obj_a(int distancia);
    uint32_t leer_control_rem();
    int  ESPERA_CONTROL();
    int  leer_simbolo_control_rem();
    int leer_numero_control_rem();
    void ejecutar_bips(int veces);
    void sonido_inicio();
    void sonido_corto();
    void sonido_ok();
    void sonido_simple();
    void sonido_fin();
    int  cargar_prog_EBLOGO();
    void ejecutar_prog_EBLOGO(int ciclos);
    //void CIR_int();
  //private: 
    void avanzar_izq();
    void avanzar_der();
    void retroceder_izq();
    void retroceder_der();
    void parar_izq();
    void parar_der();
};
 
#endif