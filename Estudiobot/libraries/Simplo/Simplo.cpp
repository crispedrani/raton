#include "Arduino.h"
#include "Simplo.h"
 
Simplo::Simplo(int pin_IR)
{
  pinMode(13, OUTPUT);    //salida zumbador
  pinMode(12, OUTPUT);    //salida sensor ultrasonico
  pinMode(10, OUTPUT);    //salida A motor izquierdo
  pinMode(8, OUTPUT);      //salida B motor izquierdo
  pinMode(9, OUTPUT);      //salida A motor derecho
  pinMode(7, OUTPUT);       //salida B motor derecho 

  pinMode(2, INPUT);          //entrada control remoto
  pinMode(3, INPUT);           //libre para encoder rueda
  pinMode(4, INPUT);           //libre para  encoder rueda
  pinMode(11, INPUT);         //entrada ultrasonico

  //A0 parlante?
}
 
void Simplo::ajustar_velocidad(int vel)
{
   vel_D= vel;
   vel_I  = vel;
}
void Simplo::iniciar()
{
  digitalWrite(13, HIGH);
  delay(500);
  digitalWrite(13, LOW);
  delay(500);
  digitalWrite(13, HIGH);
  delay(500);
  digitalWrite(13, LOW);
  delay(500);
   digitalWrite(13, HIGH);
  delay(500);
  digitalWrite(13, LOW);
  delay(500);
}

void Simplo::avanzar_izq()
{
  analogWrite (10, 255-vel_I);   
  digitalWrite(8, HIGH);       
}


void Simplo::retroceder_izq()
{
  analogWrite (10, vel_I);   
  digitalWrite(8, LOW);       
}


void Simplo::parar_izq()
{
  analogWrite (10, 0);   
  digitalWrite(8, LOW);    
}


void Simplo::avanzar_der()
{
  analogWrite (9, vel_D);   
  digitalWrite(7, HIGH);        
}


void Simplo::retroceder_der()
{
  analogWrite (9, vel_D);   
  digitalWrite(7, LOW);        
}


void Simplo::parar_der()
{
  analogWrite (9, 0);   
  digitalWrite(7, LOW);  
}


void Simplo::avanzar()
{
  avanzar_izq();
  avanzar_der();
}

void Simplo::avanzar_durante(int tiempo)
{
    avanzar();
    delay(tiempo);
    parar();
}

void Simplo::retroceder()
{
  retroceder_izq();
  retroceder_der();        
}      

void Simplo::retroceder_durante(int tiempo)
{
    retroceder(); 
    delay(tiempo);
    parar();
}

void Simplo::parar()
{
  parar_izq();
  parar_der();
}

void Simplo::parar_durante(int tiempo)
{
   parar();
   delay(tiempo);
}

void Simplo::girar_h()
{
  avanzar_izq();
  retroceder_der();
}


void Simplo::girar_ah()
{
  avanzar_der();
  retroceder_izq();
}


void Simplo::doblar_der()
{
  avanzar_izq();
  parar_der();
}

void Simplo::doblar_izq()
{
  avanzar_der();
  parar_izq();
}

void Simplo::girar_ah_durante(int tiempo)
{
   girar_ah();
   delay(tiempo);
   parar();
}

void Simplo::girar_h_durante(int tiempo)
{
   girar_h();
   delay(tiempo);
   parar();
}

void Simplo::avanzar_hasta_obj_a(int distancia)
{
   avanzar();
   while (lectura_ultrasonico() > distancia); 
   parar();  
}

void Simplo::parar_hasta_obj_a(int distancia)
{
    parar();
    while(lectura_ultrasonico()>distancia);
    //avanzar();
}

void Simplo::girar_h_hasta_obj_a(int distancia)
{
    girar_h();
    while(lectura_ultrasonico()>distancia);
    parar();
}

void Simplo::girar_ah_hasta_obj_a(int distancia)
{
    girar_ah();
    while(lectura_ultrasonico()>distancia);
    parar();
}

void Simplo::retroceder_hasta_obj_a_mas_de(int distancia)
{
   retroceder();
   while (lectura_ultrasonico() < distancia); 
   parar();  
}


bool Simplo::objeto_a_menos_de(int distancia)  
{
  if (lectura_ultrasonico() < distancia)
  {
    delay(100);
    if (lectura_ultrasonico() < distancia)  //2da comprobación con nueva medición
      return 1;
    else
      return 0;
  }
  else
    return 0;
}

int Simplo::lectura_ultrasonico()  
{
   digitalWrite(12, LOW);                
   delayMicroseconds(5);                      
   digitalWrite(12, HIGH);               
   delayMicroseconds(10);                     
   digitalWrite(12, LOW);                
   int distancia = int (pulseIn (11, HIGH) / 58);    
   return distancia;
}

uint32_t Simplo::leer_control_rem()   //espera hasta recibir comando
{
  bool lectura, ant = HIGH;
  int valor = 0;
  int pos = 0, delta, inicio;
  while(1)
  {
    lectura = digitalRead(2);
    if ((lectura == HIGH) && (ant == LOW)) 
    {
      ant = HIGH;
      //valor = 0;
      inicio = micros();         //si la salida es 1 pone en 0 a inicio
    }
    else
    if ((lectura == LOW) && (ant == HIGH))
    {
      ant = LOW;
      delta = micros() - inicio;
      if (delta < 600)   
      {
        valor <<= 1;
        valor |= 1;
        ++pos;
      }
      else 
      if (delta < 1800) 
      {
        valor <<= 1;
        valor |= 0;
        ++pos;
      }
      else 
      if (delta > 4000) 
      { 
        valor = 0;
        pos = 0;
      }
      if (pos == 32)
      {
        Serial.println(valor, HEX);
        return valor;
       }
    }
  }  
}   

int  Simplo::leer_numero_control_rem()
{
   int lectura;
   do 
  {  
    // lectura = leer_control_rem();
     lectura=ESPERA_CONTROL();
  }
  while(lectura>9); 
  //ejecutar_bips(1);
  return lectura;
}

int  Simplo::leer_simbolo_control_rem()
{
   int lectura;
   do 
  {  
     //lectura = leer_control_rem();
     lectura=ESPERA_CONTROL();
  }
  while(lectura<10 || lectura==20); //20 es error
 // ejecutar_bips(1);
  return lectura;
}

void Simplo::ejecutar_bips(int veces)
{
  for(int x=0; x<veces; x++)
  {
    digitalWrite(13, HIGH);
    delay(200);
    digitalWrite(13, LOW);
    delay(500);
  }
}

int Simplo::ESPERA_CONTROL()
{
/*
 //uint32_t valor;
  //bool lectura, ant = HIGH;

  int comando;

  //int pos = 0, delta, inicio;


  while(1)
  {
    lectura = digitalRead(2);
    if ((lectura == HIGH) && (ant == LOW)) 
    {
      ant = HIGH;
      //valor = 0;
      inicio = micros();         //si la salida es 1 pone en 0 a inicio
    }
    else
    if ((lectura == LOW) && (ant == HIGH))
    {
      ant = LOW;
      delta = micros() - inicio;
      if (delta < 600)   
      {
        valor <<= 1;
        valor |= 1;
        ++pos;
      }
      else 
      if (delta < 1800) 
      {
        valor <<= 1;
        valor |= 0;
        ++pos;
      }
      else 
      if (delta > 4000) 
      { 
        valor = 0;
        pos = 0;
      }
      if (pos == 32)
        {
        Serial.println(valor, HEX);


    if (has_value==true)
    {
        switch(leido)
        {
            case 0xFF006798:
                comando= 0;
                break;
            case  0xFF005DA2:
                comando=1;
                break;
            case  0xFF009D62:
                comando=2;
                break;
            case  0xFF001DE2:
                 comando=3;
                 break;
             case  0xFF00DD22:
                 comando=4;
                 break;
             case   0xFF00FD02:
                  comando=5;
                  break;
             case  0xFF003DC2:
                  comando=6;
                  break;
             case 0xFF001FE0:
                  comando=7;
                  break;
             case 0xFF0057A8:
                  comando=8;
                  break;
             case 0xFF006F90:
                   comando=9;
                   break;
             case 0xFF009768:  
                   comando=cod_AST;
                   break;
             case  0xFF004FB0: 
                   comando=cod_NUM;
                   break;
              case  0xFF00E718:
                   comando=cod_ARR;
                   break;
              case 0xFF00EF10:
                  comando=cod_IZQ;
                  break;
              case  0xFF00C738:
                   comando=cod_OK;
                   break;
              case 0xFF00A55A:
                   comando=cod_DER;
                   break;
              case 0xFF00B54A:
                   comando=cod_ABA;
                   break;
              default:
                   comando=20;
                   break;
            }//del switch

            if (comando!=20)
               sonido_corto();
            Serial.println(comando);
            has_value=false;
            return comando;
        }//del if has_true
  */
} 

void Simplo::sonido_ok()
{
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(50);
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(50);
}

void Simplo::sonido_simple()
{
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(50);
}   

void Simplo::sonido_inicio()
{
   digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);
}

void Simplo::sonido_fin()
{
   digitalWrite(13, HIGH);
   delay(700);
   digitalWrite(13, LOW);
}

int Simplo::cargar_prog_EBLOGO()
{
   cont=0;
   int ciclos;
    do
    {
      operacion[cont] = leer_simbolo_control_rem();
      valor[cont] = leer_numero_control_rem();
      sonido_ok();
      cont++;
    } while(operacion[cont-1]!=cod_OK); 
    sonido_ok();
    ciclos = valor[cont-1];
    return ciclos;
}

void Simplo::ejecutar_prog_EBLOGO(int ciclos)
{
for (int y=1; y<=ciclos; y++) //ciclos 1 valor[cont-1]
  {
    for (int x=0; x<cont-1; x++)    //pasos 0 a cont-1
    {
      switch (operacion[x])
      {
        case cod_ARR:
          Serial.println("avanzando");
          if (valor[x]==0)
            avanzar_hasta_obj_a(15);
          else
            avanzar_durante(valor[x]*1000);
          break;

        case cod_ABA:
          Serial.println("retrocediendo");
          if (valor[x]==0)
            retroceder_hasta_obj_a_mas_de(30);
          else
            retroceder_durante(valor[x]*1000); //agregar retro si obj a 15
          break;

        case cod_DER:
           Serial.println("girando horario");
           if (valor[x]==0)
             girar_h_hasta_obj_a(30);
           else
             girar_h_durante(valor[x]*100); //agregar girar hasta obj a 15
          break;

        case cod_IZQ:
           Serial.println("girando antihorario");
           if (valor[x]==0)
             girar_ah_hasta_obj_a(30);
           else
             girar_ah_durante(valor[x]*100); //agregar girar hasta obj a 15
          break;

        case cod_AST: 
          Serial.println("detenido");
          if (valor[x]==0)
            parar_hasta_obj_a(50);
          else
            parar_durante(valor[x]*1000);
          break;
      }
      Serial.println("fin instrucción");
    }//fin 1 ciclo
    parar();
    sonido_simple();
    Serial.println("fin ciclo");
  }//fin programa
  sonido_fin();
  Serial.println("fin programa");
}

void Simplo::sonido_corto()
{
   digitalWrite(13, HIGH);
   delay(50);
   digitalWrite(13, LOW);
}

/*
void Simplo::CIR_int()
{
  noInterrupts();
  Serial.println("rutina de atencion de interrupcion");
  if (digitalRead(2) == HIGH) 
  {
    inicio = micros();
 
   }
  else 
  { 
    delta = micros() - inicio;
   Serial.println(delta);
    if (delta < 600) 
    {
      leido <<= 1; //desplaza 1
      leido |= 1;     //carga 1
      ++pos;
    }
    else 
    if (delta < 1800) 
    {
      leido <<= 1; //desplaza 1
      leido |= 0;     //carga 0
      ++pos;          
    }
    else 
    if (delta > 4000) 
    { 
      leido = 0;
      pos = 0;
    }

   Serial.println(leido, BIN);
    if (pos == 32)
    {  
      has_value = true;
      if(leido==4278210480)
      {  
        asm("jmp 0x0000");
      }
      else
      {
        Serial.print("--->");
        Serial.println(leido);
       }
    }//else(pos==32)
  } //else(digitalRead(2)=HIGH)   
Serial.println("fin rutina atencion");
interrupts();
}*/