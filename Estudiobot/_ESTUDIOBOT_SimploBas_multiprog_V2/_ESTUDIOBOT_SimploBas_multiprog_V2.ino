#include <ir_Lego_PF_BitStreamEncoder.h>
#include <IRremote.h>

//BAS: ultrasonico - control remoto en D2 con reset por boton #

//Rutinas:
//0: prueba motores
//1: avanza y esquiva objetos  
//2: despeja zona alrededor         - ocupado por sigue linea cuando hay
//3: prog por pasos EBprog nivel 1
//4: prog por pasos EBprog nivel 2
//5: control remoto infrarrojo
//6: ocupado por WIFI cuando hay 

//entradas-salidas digitales
#define end_CIR  2  //control remoto infrarrojo
#define sal_RDB  7  //rueda derecha   B
#define sal_RIB  8  //rueda izquierda B
#define sal_RDA  9  //rueda derecha   A  salida PWM
#define sal_RIA 10  //rueda izquierda A  salida PWM
#define end_ECO 11  //ECHO del ultrasonico
#define sal_TRI 12  //TRIGGER del ultrasonico
#define sal_ZUM 13  //zumbador

//control remoto IR (modelo flechas abajo)
#define cod_0   0xFF9867
#define cod_1   0xFFA25D
#define cod_2   0xFF629D
#define cod_3   0xFFE21D
#define cod_4   0xFF22DD
#define cod_5   0xFF02FD
#define cod_6   0xFFC23D
#define cod_7   0xFFE01F
#define cod_8   0xFFA857
#define cod_9   0xFF906F
#define cod_AST 0xFF6897  //10
#define cod_NUM 0xFFB04F  //11
#define cod_ADE 0xFF18E7  //12
#define cod_ATR 0xFF4AB5  //13
#define cod_DER 0xFF5AA5  //14
#define cod_IZQ 0xFF10EF  //15
#define cod_OK  0xFF38C7  //16
#define     AST 10
#define     NUM 11
#define      OK 12
#define     ADE 13
#define     ATR 14
#define     IZQ 15
#define     DER 16

#include <IRremote.h>
#include <ir_Lego_PF_BitStreamEncoder.h>

IRrecv infrarrojo(end_CIR);           
decode_results results; 

//variables 
int vel_I = 100, vel_D = 100, numprog=0, numrutina;
int ciclos, cont, pulsos_paso, pulsos_giro;
int operacion[30], valorop[30];
uint32_t comando;

//reset por control remoto
#define LARGO_TREN 32
// En microsegundos
#define LIMITE_BAJO 600         
#define LIMITE_ALTO 1800         
#define LIMITE_MAX 4000

long inicio, delta = 0, inicio2, delta2 = 0;
uint32_t valor_devuelto, valor = 0, valor2 = 0;
int pos = 0, pos2 = 0;
boolean ant, lectura;


void setup()
{
  pinMode(end_CIR, INPUT);        //entrada control remoto
  pinMode(sal_RIA, OUTPUT);       //salida A motor izquierdo
  pinMode(sal_RIB, OUTPUT);       //salida B motor izquierdo
  pinMode(sal_RDA, OUTPUT);       //salida A motor derecho
  pinMode(sal_RDB, OUTPUT);       //salida B motor derecho 
  pinMode(end_ECO, INPUT);        //entrada pin ECHO del ultrasonico
  pinMode(sal_TRI, OUTPUT);       //salida  pin TRIGGER del ultrasonico
  pinMode(sal_ZUM, OUTPUT);       //salida zumbador

  PARAR();
  SONIDO_INICIO();
  AJUSTAR_VELOCIDAD(35);
  
  infrarrojo.enableIRIn();
  //Serial.begin(9600); //solo para debug
  
  do
  {
    numrutina = LEER_NUMERO_IR();
  }
  while(numrutina==-1);
  SONIDO_ORDEN_COMPLETA();
  switch(numrutina)
  {
    case 0:
      attachInterrupt(0, interrupcion, CHANGE);
      setup0();      //chequeo funcionamiento
      while(1)
        loop0();
      break;
      
    case 1: 
      attachInterrupt(0, interrupcion, CHANGE);
      setup1();
      while(1)
        loop1();     //esquiva obstaculos
      break;
  
    case 2:
      attachInterrupt(0, interrupcion, CHANGE);
      setup2();
      while(1)
        loop2();    //
      break;

    case 3:
      attachInterrupt(0, interrupcion, CHANGE);
      setup3();
      while(1)
        loop3();   //
      break;

    case 4:
      attachInterrupt(0, interrupcion, CHANGE);
      setup4();
      while(1)
        loop4();   //
      break;

    case 5:
      //attachInterrupt(0, interrupcion, CHANGE); //no usar simultaneamente con  control remoto
      setup5();
      while(1) 
        loop5();   //control remoto infrarrojo
      break;

    case 6: 
      attachInterrupt(0, interrupcion, CHANGE);
      setup6();
      while(1)
      loop6();   //libre
      break;

    case 7: 
      attachInterrupt(0, interrupcion, CHANGE);
      setup7();
      while(1)
        loop7();   //libre
      break;

    case 8:
      attachInterrupt(0, interrupcion, CHANGE); 
      setup8();
      while(1)
        loop8();   //libre
      break;

    case 9: 
      attachInterrupt(0, interrupcion, CHANGE);
      setup9();
      while(1)
        loop9();   //libre
      break; 
  }
}

void loop() //no se utiliza el loop
{

}


////////////////////////////////////////////////////////////////////////////////////////////

void setup0() //Rutina 0: chequeo funcionamiento
{
  
}

void loop0()
{
  AVANZAR_DURANTE(1000);
  RETROCEDER_DURANTE(1000);
  delay(1000);
  
}

//-------------------------------------------------------------------------------------------

void setup1() //Rutina 1: Esquiva objetos
{
  
}

void loop1()                 
{
  if (OBJ_A_MENOS_DE(20))         //si detecta un objeto a menos de 20 cm
  {
    PARAR();
    ACTIVAR_BIP_DURANTE(100);     //activa el zumbador
    RETROCEDER_DURANTE(300);      //retrocede un instante
    GIRAR_AH_DURANTE(300);        //gira un instante en sentido antihorario
  }
  else                            //si no detecta nada
    AVANZAR();       
}

//-------------------------------------------------------------------------------------------

void setup2() //Rutina 2: Despeja zona alrededor
{
  AJUSTAR_VELOCIDAD(40);
}

void loop2()                
{
  if(OBJ_A_MENOS_DE(40))       //si detecta un objeto a menos de 40 cm
  {
    PARAR();
    ACTIVAR_BIP_DURANTE(50);
    GIRAR_H_DURANTE(100);      //gira un poco más para quedar bien enfrentado
    AVANZAR_DURANTE(1000);     //avanza hacia el objeto para empujarlo
    RETROCEDER_DURANTE(1000);  //y retrocede
    GIRAR_H_DURANTE(100);      //se corre para no quedar enfrente del objeto otra vez
    ESPERAR(500);
  }
  else                         //si no detecta nada
  {
    GIRAR_H_DURANTE(100);      //gira 
    ESPERAR(100);              //y para un instante, asi gira haciendo movimientos cortos
  } 
}                             

//--------------------------------------------------------------------------------------------

void setup3() //Rutina 3: Ebprog Nivel1_T (pasos de tiempos fijos -solo flechas-)
{
  ciclos = CARGAR_EBPROG_N1_T(); //N1: solo flechas
  SONIDO_ORDEN_COMPLETA();
}

void loop3()                 
{
  EJECUTAR_EBPROG_N1_T();
  do
  { 
    comando = LEER_SIMBOLO_IR();
  }  
  while(!(comando==OK));
  SONIDO_ORDEN_COMPLETA();
}

//---------------------------------------------------------------------------------------------

void setup4() //Rutina 4: Ebprog Nivel2_T (pasos de tiempos ajustables -flechas y numeros-)
{
  ciclos = CARGAR_EBPROG_N2_T(); 
  SONIDO_ORDEN_COMPLETA();
}

void loop4()
{
  EJECUTAR_EBPROG_N2_T(ciclos);
  do
  { 
    comando = LEER_SIMBOLO_IR();
  }  
  while(!(comando==OK));
  SONIDO_ORDEN();
  ciclos = LEER_NUMERO_IR();
  if (ciclos==0)
    ciclos=10000;
  SONIDO_ORDEN_COMPLETA();
}

//---------------------------------------------------------------------------------------------

void setup5() //Rutina 5: Control remoto infrarrojo
{
  
}

void loop5()                
{
  switch(LEER_COMANDO_IR())
  {
    case ADE:
      ACTIVAR_BIP_DURANTE(50);
      AVANZAR();
      break;
  
    case ATR:
      ACTIVAR_BIP_DURANTE(50);
      RETROCEDER();
      break;

    case DER:
      ACTIVAR_BIP_DURANTE(50);
      GIRAR_H_DURANTE(200);
      break;

    case IZQ: 
      ACTIVAR_BIP_DURANTE(50);
      GIRAR_AH_DURANTE(200);
      break;

    case OK:
      PARAR();
      ACTIVAR_BIP_DURANTE(150);
      break;

    case AST: 
      ACTIVAR_BIP_DURANTE(200);
      PARAR();
      break;

    case NUM:
      RESETEAR();
      break;
  }
}

//------------------------------------------------------------------------------------------------

void setup6() //Rutina 6: Libre
{
  //programar aquí el setup de la rutina nro. 6
}

void loop6()
{
  //programar aquí el loop de rutina nro. 6
}

//-------------------------------------------------------------------------------------------------

void setup7() //Rutina 7: Libre
{
  //programar aquí el setup de la rutina nro. 7
}

void loop7()
{
  //programar aquí el loop de rutina nro. 7
}


//-------------------------------------------------------------------------------------------------

void setup8() //Rutina 8: Libre
{
  //programar aquí el setup de la rutina nro. 8
}

void loop8()
{
  //programar aquí el loop de rutina nro. 8
}


//-------------------------------------------------------------------------------------------------

void setup9() //Rutina 9: Libre
{
  //programar aquí el setup de la rutina nro. 9
}

void loop9()
{
  //programar aquí el loop de rutina nro. 9 
}


///////////////////////////////////////////////////////////////////////////////////////////////////

int DIST_MEDIDA()  
{
  digitalWrite(sal_TRI, LOW);                
  delayMicroseconds(5);                      
  digitalWrite(sal_TRI, HIGH);               
  delayMicroseconds(10);                     
  digitalWrite(sal_TRI, LOW);                
  int distancia = int (pulseIn (end_ECO, HIGH) / 58);    
  return distancia;
}


bool OBJ_A_MENOS_DE(int distancia)  
{
  if (DIST_MEDIDA() < distancia)
  {
    delay(100);
    if (DIST_MEDIDA() < distancia)  //2da comprobación con nueva medición
      return 1;
    else
      return 0;
  }
  else
    return 0;
}


void AJUSTAR_VELOCIDAD(int vel)
{
  vel_I = map(vel, 0, 100, 70, 150);
  vel_D = vel_I;
}

void AVANZAR_IZQ()
{
  analogWrite (sal_RIA, 255-vel_I);   
  digitalWrite(sal_RIB, HIGH);       
}

void RETROCEDER_IZQ()
{
  analogWrite (sal_RIA, vel_I);   
  digitalWrite(sal_RIB, LOW);       
}

void PARAR_IZQ()
{
  analogWrite (sal_RIA, 0);   
  digitalWrite(sal_RIB, LOW);    
}

void AVANZAR_DER()
{
  analogWrite (sal_RDA, 255-vel_D);   
  digitalWrite(sal_RDB, HIGH);        
}

void RETROCEDER_DER()
{
  analogWrite (sal_RDA, vel_D);   
  digitalWrite(sal_RDB, LOW);        
}

void PARAR_DER()
{
  analogWrite (sal_RDA, 0);   
  digitalWrite(sal_RDB, LOW);  
}

void ESPERAR(int tiempo)
{
  for(int t=0; t<tiempo; t++)
  {
    delay(1);
  }       
}

void AVANZAR()
{
  AVANZAR_IZQ();
  AVANZAR_DER();
}

void RETROCEDER()
{
  RETROCEDER_IZQ();
  RETROCEDER_DER();        
}      

void PARAR()
{
  PARAR_IZQ();
  PARAR_DER();
}

void GIRAR_H()
{
  AVANZAR_IZQ();
  RETROCEDER_DER();
}

void GIRAR_AH()
{
  RETROCEDER_IZQ();
  AVANZAR_DER();
}

void DOBLAR_DER()
{
  AVANZAR_IZQ();
  PARAR_DER();
}

void DOBLAR_IZQ()
{
  AVANZAR_DER();
  PARAR_IZQ();
}

void AVANZAR_DURANTE(int tiempo)
{
  AVANZAR();
  ESPERAR(tiempo);
  PARAR();
}

void RETROCEDER_DURANTE(int tiempo)
{
  RETROCEDER();
  ESPERAR(tiempo);
  PARAR();      
}      

void PARAR_DURANTE(int tiempo)
{
  PARAR();
  ESPERAR(tiempo);
}

void GIRAR_H_DURANTE(int tiempo)
{
  GIRAR_H();
  ESPERAR(tiempo);
  PARAR();
}

void GIRAR_AH_DURANTE(int tiempo)
{
  GIRAR_AH();
  ESPERAR(tiempo);
  PARAR();
}

void DOBLAR_DER_DURANTE(int tiempo)
{
  DOBLAR_DER();
  ESPERAR(tiempo);
  PARAR();
}

void DOBLAR_IZQ_DURANTE(int tiempo)
{
  DOBLAR_IZQ();
  ESPERAR(tiempo);
  PARAR();
}


void interrupcion() 
{
  noInterrupts();
  if (digitalRead(end_CIR) == HIGH)  
    inicio2 = micros();              
  else 
  { 
    delta2 = micros() - inicio2;     
 
    if (delta2 < 700)                 
    {                                
      valor2 <<= 1;                
      valor2 |= 1;
      ++pos2;
    }
    else 
    if (delta2 < 1800) //1800        
    {
      valor2 <<= 1;
      valor2 |= 0;
      ++pos2;
    }
    else 
    if (delta2 > 4000)                
    { 
      valor2 = 0;
      pos2 = 0;
    }

    if (pos2 == 32)
    {  
      if(valor2==4278210480) 
        asm("jmp 0x0000");
    }
  }   
  interrupts();
}

void RESETEAR()
{
  asm("jmp 0x0000");  
}


uint32_t LEER_COMANDO_IR()
{
  uint32_t lect;
  int num=-1;
  while(num==-1)
  {
    if (infrarrojo.decode(&results))
    {
      lect = results.value;
      infrarrojo.resume(); 
      num = valorNum(lect);
    }
  }
  return num;
}

int LEER_SIMBOLO_IR()
{
  int lect;
  do
  {
    lect = LEER_COMANDO_IR();
  }while(lect<10);
  return lect;
}

int LEER_NUMERO_IR()
{
  int lect;
  do
  {
    lect = LEER_COMANDO_IR();
  }while(lect>9);
  return lect;
}

int valorNum(uint32_t num32)
{
  switch(num32)
  {
    case cod_0:
      return 0;
    case cod_1:
      return 1;
    case cod_2:
      return 2;
    case cod_3:
      return 3;
    case cod_4:
      return 4;
    case cod_5:
      return 5;
    case cod_6:
      return 6;
    case cod_7:
      return 7;
    case cod_8:
      return 8;
    case cod_9:
      return 9;
    case cod_AST:
      return 10;
    case cod_NUM:
      return 11;
    case cod_OK:
      return 12;
    case cod_ADE:
      return 13;
    case cod_ATR:
      return 14;
    case cod_IZQ:
      return 15;
    case cod_DER:
      return 16;
    default:
      return -1;
  }
}


void SONIDO_INICIO()
{
 for(int cont=0; cont<3; cont++)
  {
    ACTIVAR_BIP_DURANTE(100);
    delay(250);
  }  
}

void SONIDO_ORDEN()
{
  ACTIVAR_BIP_DURANTE(100);
}

void SONIDO_ORDEN_COMPLETA()
{
  ACTIVAR_BIP_DURANTE(100);
  delay(200);
  ACTIVAR_BIP_DURANTE(100);
}

void SONIDO_FIN()
{
  ACTIVAR_BIP_DURANTE(300);
}


void ACTIVAR_BIP_DURANTE(int tiempo)
{
  digitalWrite(sal_ZUM, HIGH);
  delay(tiempo);
  digitalWrite(sal_ZUM, LOW);
}

void ACTIVAR_BIP()
{
  digitalWrite(sal_ZUM, HIGH);
}

void DESACTIVAR_BIP()
{
  digitalWrite(sal_ZUM, LOW);
}


int CARGAR_EBPROG_N2_T()
{
  cont=0;
  int ciclos;
  do
  {
    operacion[cont] = LEER_SIMBOLO_IR();
    SONIDO_ORDEN();
    valorop[cont] = LEER_NUMERO_IR();
    SONIDO_ORDEN_COMPLETA();
    cont++;
  }while(operacion[cont-1]!=OK);
  SONIDO_ORDEN_COMPLETA();
  ciclos = valorop[cont-1];
  if (ciclos==0)
    ciclos = 1000;
  return ciclos;
}

void EJECUTAR_EBPROG_N2_T(int ciclos)
{
for (int y=1; y<=ciclos; y++) //ciclos 1 valor[cont-1]
  {
    for (int x=0; x<cont-1; x++)    //pasos 0 a cont-1
    {
      switch (operacion[x])
      {
        case ADE: //decia cod_ARR
          if (valorop[x]==0)
          {
            AVANZAR();
            while (!OBJ_A_MENOS_DE(30));
            PARAR();
          }
          else
            AVANZAR_DURANTE(valorop[x]*1000); //1 a 9 seg
          break;

        case ATR: 
          if (valorop[x]==0)
          {
            RETROCEDER();
            while (OBJ_A_MENOS_DE(20));
            PARAR();
          }
          else
            RETROCEDER_DURANTE(valorop[x]*1000); //1 a 9 seg
          break;

        case DER: 
           if (valorop[x]==0)
           {
             GIRAR_H();
             while (!OBJ_A_MENOS_DE(20));
             PARAR();
           }  
           else
             GIRAR_H_DURANTE(valorop[x]*100); //0,1 a 0,9 seg
          break;

        case IZQ: 
           if (valorop[x]==0)
           {
             GIRAR_AH();
             while (!OBJ_A_MENOS_DE(20));
             PARAR();
           } 
           else 
             GIRAR_AH_DURANTE(valorop[x]*100); //0,1 a 0,9 seg
          break;

        case AST: 
          if (valorop[x]==0)
          {
            PARAR();
            while (!OBJ_A_MENOS_DE(50));
          }  
          else
            PARAR_DURANTE(valorop[x]*1000); //1 a 9 seg
          break;
      }
    }//fin 1 ciclo
    PARAR();
    SONIDO_ORDEN();
  }//fin programa
  SONIDO_FIN();
}
 

int CARGAR_EBPROG_N1_T()
{
  cont=0;
  do
  {
    operacion[cont] = LEER_SIMBOLO_IR();
    SONIDO_ORDEN_COMPLETA();
    cont++;
  }while(operacion[cont-1]!=OK);
  SONIDO_ORDEN_COMPLETA();
}

void EJECUTAR_EBPROG_N1_T()
{
  for (int x=0; x<cont-1; x++)    //pasos 0 a cont-1
  {
    switch (operacion[x])
    {
      case ADE: 
        AVANZAR_DURANTE(1000);
        break;

      case ATR: 
        RETROCEDER_DURANTE(1000);
        break;

      case DER: 
        GIRAR_H_DURANTE(400); //tiempo aprox para giro de 90 grados aprox con bateria cargada
        break;

      case IZQ:
        GIRAR_AH_DURANTE(400); //tiempo aprox para giro de 90 grados aprox con bateria cargada
        break;

      case AST: 
        PARAR_DURANTE(1000);
        break;
    }
    PARAR();
    delay(1000);//tiempo entre pasos
  }//fin programa
  SONIDO_FIN();
}

 
  
