#https://pyglet.readthedocs.io/en/latest/programming_guide/quickstart.html
#anchor_x='center', anchor_y='center'

# pone disponible las librerías
import pyglet
from pyglet.window import key

#crea la ventana
window = pyglet.window.Window()

#creo el personaje
image = pyglet.resource.image('auto.png')

@window.event
def on_draw():
    window.clear()
    image.blit(0, 0)


#hace que se ejecute constantemente, hasta que cierres la ventana
pyglet.app.run()