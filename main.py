#https://pyglet.readthedocs.io/en/latest/programming_guide/quickstart.html

import pyglet
from pyglet.window import key
from pyglet.gl import *
import random
import math

width = 960
height = 540

glEnable(GL_BLEND)
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

window = pyglet.window.Window(width, height)
image = pyglet.resource.image('auto.png')
image.anchor_x = image.width / 2
image.anchor_y = image.height / 2


#cars = [{'x': 0, 'y': 0},{'x': 200, 'y': 0}]
cars = []
sprites = {}

@window.event
def on_draw():
    window.clear()
    for car in cars:
        sprite = sprites[car["symbol"]]
        sprite.x = car["x"]
        sprite.y = car["y"]
        sprite.rotation = car["rotation"]

        sprite.draw()


@window.event
def on_key_press(symbol, modifiers):
    for car in cars:
        if symbol == car['symbol']:
            car['press'] = True 
            return
    cars.append({
        "x" : random.randint(0, width),
        "y" : random.randint(0, height),
        "symbol" : symbol, 
        "speed" : 50,
        "press" : False,
        "radio" : 12,
        "live" : True,
        "rotation" : random.randint(0, 360),
    })
    sprites[symbol] = pyglet.sprite.Sprite(image, 0, 0, )
    

@window.event
def on_key_release(symbol, modifiers):
    for car in cars:
        if symbol == car['symbol']:
            car['press'] = False 
            return

def update(dt):
    global cars
    for car in cars:
        car["x"] += math.cos(car["rotation"] * math.pi / 180) * car["speed"] * dt
        car["y"] -= math.sin(car["rotation"] * math.pi / 180) * car["speed"] * dt

        if car["press"]:
            car["rotation"] += 200 * dt
        
        if car["x"] > width + 30 :
            car["x"] = -30
        if car["x"] < -30:
            car["x"] = width

        if car["y"] > height + 30:
            car["y"] = -30
        if car["y"] < -30:
            car["y"] = height
        
        for other in cars:
            if other['symbol'] != car['symbol']:
                
                d = math.sqrt(math.pow(car["x"] - other["x"],2) + math.pow(car["y"] - other["y"],2))
                if d < car["radio"] + other["radio"]:
                    other["live"] = False
                    car["live"] = False
                    print("choque")
    #for car in cars:
    #    if car["live"] == False:
    #        cars.remove(value)

pyglet.clock.schedule_interval(update, 1/60.0)
pyglet.app.run()
